/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017.
 *
 * @file    Scrambler.cpp
 * @authors Jim Daehn <jdaehn@missouristate.edu>
 *          Trenton Sauer <ts191@live.missouristate.edu>
 *			Jay Trivedi <jay1609@live.missouristate.edu>
 * @brief   Scrambler implementation.
 *
 * @copyright Jim Daehn, 2017. All rights reserved
 */


#include <sstream>

#include "Scrambler.h"

/**
 * @brief Encapsulates ADTs developed in CSC232, Data Structures with C++.
 */
namespace csc232 {
    Scrambler::Scrambler(const char a[], index beginIndex, index endIndex)
            : begin(beginIndex), end(endIndex) {
		for(index i = begin; i < end; ++i){
			data[i] = a[i];
		}
    }

    size_t Scrambler::length() const {
		size_t arrayLength = 0;
		for(index i = begin; i <= end; ++i){
			arrayLength = i;
		}
		return arrayLength;
    }

    std::string Scrambler::scramble() const {
		std::stringstream ss;
		std::string scrambledString;
		reverse(data, ss, begin, end -1);
		ss >> scrambledString;
        return scrambledString;
    }

    std::string Scrambler::toString() const {
		std::string dataString;
		for(index i = begin; i < end; ++i){
			dataString += data[i];
		}
        return dataString;
    }


    Scrambler::~Scrambler() {
        // No op... leave me alone.
    }


    void Scrambler::reverse(const char anArray[], std::stringstream& ss,
                            const int first, const int last) const {
		if (first <= last){
			ss << anArray[last];
			reverse(anArray, ss, first, last - 1);
		}
    }
}
